<?php
// Nom de l'utilisateur actuel
$utilisateur = shell_exec('whoami');

// Distribution utilisée
$distribution = shell_exec('lsb_release -d -s');

// Version de noyau Linux utilisée
$versionNoyau = shell_exec('uname -r');

// Taille de la mémoire RAM disponible et totale
$ramTotal = shell_exec('free -h | grep "Mem:" | awk \'{print $2}\'');
$ramDisponible = shell_exec('free -h | grep "Mem:" | awk \'{print $7}\'');

// Taille de stockage disponible et totale
$stockageTotal = disk_total_space("/");
$stockageDisponible = disk_free_space("/");

// Affichage des informations
echo "<h2>Informations du serveur :</h2>";
echo "<p>Nom de l'utilisateur : " . $utilisateur . "</p>";
echo "<p>Distribution utilisée : " . $distribution . "</p>";
echo "<p>Version de noyau Linux : " . $versionNoyau . "</p>";
echo "<p>Taille de la mémoire RAM totale : " . $ramTotal . "</p>";
echo "<p>Taille de la mémoire RAM disponible : " . $ramDisponible . "</p>";
echo "<p>Taille de stockage totale : " . formatSizeUnits($stockageTotal) . "</p>";
echo "<p>Taille de stockage disponible : " . formatSizeUnits($stockageDisponible) . "</p>";

// Fonction pour formater la taille en unités lisible
function formatSizeUnits($size)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB');
    $i = 0;
    while ($size >= 1024 && $i < 4) {
        $size /= 1024;
        $i++;
    }
    return round($size, 2) . ' ' . $units[$i];
}
?>
